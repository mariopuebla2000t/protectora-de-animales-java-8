package Persistencia;

import java.io.*;
import java.util.*;

public class Main { //Clase principal / main.
	
	static Scanner TC = new Scanner(System.in); // Creamos un Scanner para poder introducir datos por consola.
	
	public static void main(String []args) throws FileNotFoundException{ //Excepcion que nos sirve para cuando no se encuentre el fichero por el nombre del mismo o la ruta, de un mensaje de error.
		Protectora ptt=new Protectora("Cuidales",20);//Creamos una clase protectora con un Nombre para la empresa y un NUMERO DE ANIMALES MAXIMOS QUE PUEDE ALBERGAR.
		ClinicaVeterinaria cv=new ClinicaVeterinaria("Curara",925234567,50.0); // Creamos una Clase ClinicaVeterinaria en donde Introducimos su Nombre de empresa, su telefono y EL PRECIO DE ESTERILIZACION DE LAS GATAS.
		Ayuntamiento ay=new Ayuntamiento(925457309,20.0); // Creamos una Clase Ayuntamiento en donde pondremos el numero de telefono y LA CANTIDAD DE SUBVENCION.
		leerAnimales("Animales.txt",ptt);//Metodo que va a leer los animales del fichero con ese nombre.
	    menuPrincipal(ptt,ay,cv);//Metodo que nos mostrara las diferentes opciones a realizar en un menú.
	}

	public static void menuPrincipal(Protectora ptt,Ayuntamiento ay, ClinicaVeterinaria cv) throws InputMismatchException {
	//Metodo de las opciones que lanzara una excepcion por si a la hora de elegir numero de opcion se escribe una letra.
		int opcionMenu=0; //Declaracion de distintos elementos que se van a utilizar en cada opcion.
		boolean repetirMenu=true; //Variable para repetir el menu si sale la excepcion.
		while(repetirMenu) {//Bucle que repite el menu.
			repetirMenu=false;
			System.out.println("Bienvenido al programa de gestión de La Protectora de Animales.");
			System.out.println("Seleccione una opcion del menu:\n 1-Mostrar informacion de los animales.\n 2-Realizar solicitud de adopcion/acogida."
					+ "\n 3-Consultar numero de solicitudes de adopcion de un animal. \n 4-Calculo de los costes veterinarios anuales."
					+ "\n 5-Calculo de la campania de esterilizacion de gatas. \n 6-Cantidad (kg) de pienso PerroAdulto para una semana. \n 7-Calculo subvencion del ayuntamiento."
					+ "\n 8-Salir de programa.");//Datos para que sepa el usuario que opcion elegir,
			try {					
				opcionMenu=TC.nextInt();//Opcion que elige el usuario recogida por el teclado dentro del rango.		
				if(opcionMenu<1 || opcionMenu>8) { //Hemos implementado un if que solo dejará continuar si se introduce un numero entre el 1 y el 8.
					repetirMenu=true;
					throw new FueraIntervaloException(); //Aqui implementamos la primera excepcion.
				}
			
				switch (opcionMenu) { //Ponemos los distintos casos que puede elegir y dependiendo de la respuesta, se elige una opcion a realizar.
				case 1: 
					System.out.println(ptt.devolverDatosAnimales()); //Muestra la informacion de todos los animales.
					System.out.println("");
					menuPrincipal(ptt,ay,cv); //Vuelve a mostrar el menu despues de ejecutar la opcion, asi para todos los casos.
					break;
	
				case 2:
					String nombreA=""; //Esta variable servirá para poder escribir por teclado el nombre del animal al cual queremos realizar una solicitud.
					boolean realizarSolic; 
					try {
						nombreA=comprobarNombreA(ptt); //Comprueba si el nombre introducido esta en la matriz o no.
						realizarSolic=ptt.comprobacionSolicitud(nombreA); //Mostrar y comprobar cuantas solicitudes hay en un animal.
						if(realizarSolic==false) { 
							throw new DiezSolicitudesException(); 
						}
						System.out.println("Tras las comprobaciones, podemos realizar una solicitud.");
						System.out.println("Introduzca el tipo de solicitud a realizar, siendo 0 acogida y 1 adopcion:");
						int tipoS=TC.nextInt();
						if(tipoS<0 || 1<tipoS) {
							throw new FueraIntervaloException(); //Aqui tenemos la tercera excepcion, la cual da un mensaje de error si no se introduce el parametro correcto.
						}
						System.out.println("Introduzca su numero de telefono");
						String telfSolicitante=TC.next();
						System.out.println("Introduciendo solicitud....");
						ptt.addSolicitud(tipoS, telfSolicitante, nombreA); //Se guarda la solicitud asignada al animal.
						System.out.println("Usted ha realizado la solicitud con exito");
						System.out.println("");
					}catch (DiezSolicitudesException d) { //Aqui podemos ver las excepciones bien implementadas.
						System.out.println("Error:\nSolo se pueden 10 solicitudes por animal\n");
					}catch (FueraIntervaloException q) {
						System.out.println("Opcion no disponible. Debe introducir un numero entre 0 y 1\n");
					}
					menuPrincipal(ptt,ay,cv);//Volver a mostrar el menu.
					break;
				
				case 3:
					int nSolicitudes;
					System.out.println("Sistema de comprobacion de solicitudes de adopcion");
					nombreA=comprobarNombreA(ptt); //Este método comprueba si el nombre introducido por teclado esta en la matriz o no.
					System.out.println("Realizando comprobaciones...");
					nSolicitudes=ptt.consultarSolicitudesAdop(nombreA); // Este metodo se encaga de comprobar las solicitudes tiene el animal asociado.
					System.out.println(nombreA+" tiene: "+nSolicitudes+" solicitudes");
					System.out.println(ptt.devolverSolicitudes(nombreA)); //Este metodo se encarga de imprimir todas las solicitudesa asignadas a un animal.
					System.out.println("");
					menuPrincipal(ptt,ay,cv);//Volver a mostrar el menu.
					break;
				case 4:
					System.out.println("Sistema de calculo de gastos veterinarios");
					double gastosVet=ptt.calcularTotalGastosVeterinariosAnuales(); // Hace un sumatorio de todos los gastos realizados a los perros y a los gatos.
					System.out.println("Gastos: "+gastosVet+" euros");
					System.out.println("");
					menuPrincipal(ptt,ay,cv); //Volver a mostrar el menu.
					break; 
				case 5:
					double costeCamp;
					System.out.println("Sistema de calculo de la campania de esterilizacion de gatas");
					costeCamp=ptt.calculoCampaniaEsterilizacion(cv); //Recorre la matriz para ver la cantidad de gatas que hay Esterilizadas
					System.out.println(cv.toString()); //Imprimimos los datos asociados a la Clinica Veterinaria.
					System.out.println("El coste de la campania es: "+costeCamp+" euros.");
					System.out.println("");
					menuPrincipal(ptt,ay,cv); //Volver a mostrar el menu.
					break;
				case 6:
					double cantidad;
					System.out.println("Sistema de calculo de pienso para los perros");
					System.out.println("Cantidad (kg) de pienso PerroAdulto para una semana");
					cantidad=ptt.calcularCantidadPiensoAdultos(); //Calcular la cantidad de pienso total que se va a suministrar a todos perros.
					System.out.println("La cantidad de pienso es: "+cantidad+" kg");
					System.out.println("");
					menuPrincipal(ptt,ay,cv); //Volver a mostrar el menu.
					break;
				case 7:
					double cantSubTotal;
					System.out.println("Sistema de calculo de subvencion del Ayuntamiento");
					cantSubTotal=ptt.calcularSubvencionAyuntamiento(ay); //Calcular el total de subvencion que va a proporcionar el ayuntamiento.
					System.out.println(ay.toString());  //Imprimimos los datos asociados al ayuntamiento.
					System.out.println("El ayutamiento ofrece una subvencion de: "+cantSubTotal+" euros.");
					System.out.println("");
					menuPrincipal(ptt,ay,cv); //Volver a mostrar el menu.
					break;
				case 8:
					System.out.println("Cerrando protectora...");
					TC.close(); //Cerramos el Scanner que servia para introducir los datos.
					System.exit(1);//Salir del programa.
					break;	
				}
			}catch(FueraIntervaloException f) { // Implementamos aqui las otras excepciones que nos faltaban.
				System.out.println("Opcion no disponible. Introduzca un numero entre 1 y 8\n");
			}catch(InputMismatchException i) {
				repetirMenu=true;
				TC.nextLine();
				System.out.println("Error: Introduzca un numero entre las opciones disponibles\n");
			}
			}
	}


	public static void leerAnimales(String cadena, Protectora ptt)throws FileNotFoundException{ // Este metodo se utiliza al principio para poder leer el fichero "Animales.txt"
		try {		
			File f=new File(cadena);  // El archivo, al estar con los archivos, coge esa direccion directamente sin preguntarla.  		
			Scanner nombre_f = new Scanner (f);    		
			while (nombre_f.hasNext()){    			
				char tipoAnimal=nombre_f.next().charAt(0);      			
				if (tipoAnimal=='p'){        				
					String nombre=nombre_f.next();  				
					char sexo= nombre_f.next().charAt(0);				
					int anios=nombre_f.nextInt();         				
					boolean sociablePersonas =nombre_f.nextBoolean();       				
					boolean apadrinado = nombre_f.nextBoolean();   					
					String raza=nombre_f.next();       				
					int tamanioKg=nombre_f.nextInt();      				
					boolean ppp=nombre_f.nextBoolean();       				
					boolean leishmania=nombre_f.nextBoolean();      				
					Animal a = new Perro(nombre,sexo,anios,sociablePersonas,apadrinado,raza,tamanioKg,ppp,leishmania);        				
					ptt.addAnimal(a);     			
				}     			
				else if(tipoAnimal=='g') {        				
					String nombre=nombre_f.next();       				
					char sexo= nombre_f.next().charAt(0);        			
					int anios=nombre_f.nextInt();       				
					boolean sociablePersonas =nombre_f.nextBoolean();        				
					boolean apadrinado = nombre_f.nextBoolean();    					
					boolean esterilizado=nombre_f.nextBoolean();	    					
					Animal a = new Gato(nombre,sexo,anios,sociablePersonas,apadrinado,esterilizado);  					
					ptt.addAnimal(a);			
				}		
			}
			nombre_f.close(); //Cerramos el Scanner que servia para leer el fichero.
		
		}catch(FileNotFoundException e) { //Respuesta de excepcion.
			  System.out.println("Error:\nFichero no encontrado");
			  System.exit(1); //Fin del programa.
		}
	}

	public static String comprobarNombreA(Protectora ptt) { //Este método comprueba si el nombre introducido por teclado esta en la matriz o no.
		boolean comprobar=false;
		String nombreA="";
		do {
			System.out.print("Introduzca nombre del animal: ");
			nombreA=TC.next();
			if(!ptt.verificarNombre(nombreA)) { //Este método, recorre la matriz y compara el nombre introducido con los de la matriz.
				comprobar=true;
			}else {
				comprobar=false;
			}
		}while(comprobar);
		return nombreA;
	}
}
