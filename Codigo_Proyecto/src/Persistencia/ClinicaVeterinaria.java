package Persistencia;

public class ClinicaVeterinaria {
	
	String nombre;
	int numTelf;
	double precioEstGata;
	
	ClinicaVeterinaria(String nombre, int numTelf, double precioEstGata){
		
		this.nombre=nombre;
		this.numTelf=numTelf;
		this.precioEstGata=precioEstGata;
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre=nombre;
	}

	public int getNumTelf() {
		return numTelf;
	}

	public void setNumTelf(int numTelf) {
		this.numTelf=numTelf;
	}

	public double getPrecioEstGata() {
		return precioEstGata;
	}

	public void setPrecioEstGata(double precioEstGata) {
		this.precioEstGata=precioEstGata;
	}
	
	public String toString() {
		return "Clinica veterinaria "+nombre+":\nN� Telefono: "+numTelf+". El precio que ofrecen para esterilizar por gata es de "+precioEstGata+" �";
	}
}
