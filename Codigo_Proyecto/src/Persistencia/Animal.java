package Persistencia;

public abstract class Animal implements Interfaz {
	
	String nombre;
	char sexo;
	int anios;
	boolean sociablePersonas;
	boolean apadrinado;
	int nSolicitudes;
	Solicitud [] solicitudes; 
	
    Animal(String nombre, char sexo, int anios, boolean sociablePersonas, boolean apadrinado) {
		
		this.nombre = nombre;
		this.sexo = sexo;
		this.anios = anios;
		this.sociablePersonas = sociablePersonas;
		this.apadrinado = apadrinado;
		solicitudes = new Solicitud[10];
		nSolicitudes=0;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public int getAnios() {
		return anios;
	}

	public void setAnios(int anios) {
		this.anios = anios;
	}

	public boolean getSociablePersonas() {
		return sociablePersonas;
	}

	public void setSociablePersonas(boolean sociablePersonas) {
		this.sociablePersonas = sociablePersonas;
	}

	public boolean getApadrinado() {
		return apadrinado;
	}

	public void setApadrinado(boolean apadrinado) {
		this.apadrinado = apadrinado;
	}
	
	public void addSolicitud(Solicitud s) { // Se aniade una solicitud a ese animal encontrado.
		if(nSolicitudes<solicitudes.length) {
			solicitudes[nSolicitudes]=s;
			nSolicitudes++; 
		}
	}
	
	public boolean realizarSolicitud() {  //Almacena las solicitudes e impide que supere el numero maximo de solicitudes establecidas
		boolean realizarSolic=true;
		if(MAX_SOLICITUDES<=nSolicitudes) {
			realizarSolic=false;
		}
		return realizarSolic;
	}
	
	public int consultarSolicitudes() { //Este metodo comprueba cuantas solicitudes tiene el animal asociado.
		return nSolicitudes;
	}

	public abstract double calculoGastosVetAnual(); // Este metodo abstracto es el que se sobreescribira en las clases de Perro y Gato para obtener el dinero que se gasta en ellos

	public String devolverSolicitudes() { //Este metodos se encarga de llevar la informacion de las solicitudes de un animal.
		String info="";
		for(int i=0;i<nSolicitudes;i++) {
			info = info+ "Solicitud "+(i+1)+" : [Es una solicitud de tipo: "+solicitudes[i].getTipoSolicitud()+
					". El teléfono del solicitante: "+solicitudes[i].getTelfSolicitante()+"]\n";	
		}
		return info;
	}
}

