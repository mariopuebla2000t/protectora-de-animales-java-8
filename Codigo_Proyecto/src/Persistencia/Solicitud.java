package Persistencia;

public class Solicitud {
	
	int tipoSolicitud;
	String telfSolicitante;
	
	Solicitud(int tipoSolicitud, String telfSolicitante){
		
		this.tipoSolicitud=tipoSolicitud;
		this.telfSolicitante=telfSolicitante;
		
	}

	public int getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(int tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	public String getTelfSolicitante() {
		return telfSolicitante;
	}

	public void setTelfSolicitante(String telfSolicitante) {
		this.telfSolicitante = telfSolicitante;
	}
		
}
