package Persistencia;

public class Gato extends Animal{
	
	boolean esterilizado;
	
	Gato(String nombre, char sexo, int anios, boolean noSociablePersonas, boolean apadrinado, boolean esterilizado){
		
		super(nombre, sexo, anios, noSociablePersonas, apadrinado);
		this.esterilizado=esterilizado;
			
	}

	public boolean getEsterilizado() {
		return esterilizado;
	}

	public void setEsterilizado(boolean esterilizado) {
		this.esterilizado = esterilizado;
	}
	
	public double calculoGastosVetAnual() { // Este es el metodo sobreescrito del que se hablaba en la clase protectora que utiliza una formula para calcular el precio.
		double gastosVetAnual=0;
		if(apadrinado==false) {
			if(esterilizado==false && sexo=='h') {
				gastosVetAnual=TRAT_CELO_GATOS*12;
			}
		}
		return gastosVetAnual;
	}

	public String toString() {
		return "Gato (Nombre : "+nombre+", Sexo : "+sexo+", A�os :"+anios+", Sociable : "+sociablePersonas+", Apadrinado: "+apadrinado+", Esterilizado: "+esterilizado+")";
	}	
}
