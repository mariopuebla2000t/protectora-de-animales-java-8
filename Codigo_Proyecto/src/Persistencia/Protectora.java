package Persistencia;

public class Protectora implements Interfaz{

	String nombre;
	int nAnimales;
	Animal[] listaAnimales;

	Protectora(String nombre, int nAnimales) {

		this.nombre = nombre;
		listaAnimales = new Animal[20];
		nAnimales=0;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void addAnimal(Animal a) { //Aniadir un animal a la protectora
		if(nAnimales< listaAnimales.length) {
			listaAnimales[nAnimales]= a;
			nAnimales++;
		}
	}

	public String devolverDatosAnimales() { // Muestra la informacion de todos los animales.
		String info="";
		for(int i =0;i<nAnimales;i++) {
			info = info+ "Animal "+(i+1)+" : "+listaAnimales[i].toString()+"\n"; 		
		}
		return info;
	}

	public void addSolicitud(int tipoSolicitud, String telfSolicitante, String nombreA) { //Añadir una solicitud para un animal.
			Solicitud s = new Solicitud (tipoSolicitud,telfSolicitante);
			for(int i=0; i<nAnimales;i++) {
				if(listaAnimales[i].getNombre().equalsIgnoreCase(nombreA)) { //Vemos si el nombre es igual a alguno de los que se aniadieron a los animales.
					Animal a =listaAnimales[i];
					a.addSolicitud(s);//En el caso de que si, se aniade una solicitud a ese animal encontrado.

				}
			}
	}

	public boolean verificarNombre(String nombreA) { //Este método, recorre la matriz y compara el nombre introducido con los de la matriz.
		boolean verificado=false;
		for(int i=0;i<nAnimales;i++) {
			if(listaAnimales[i].getNombre().equalsIgnoreCase(nombreA)) { // Utiliza el metodo de coger el nombre y utiliza "equalsIgnoreCase" para distingir entre mayusculas y minusculas.
				verificado=true;
			}	
		}	
		return verificado;
	}

	public boolean comprobacionSolicitud(String nombreA) { //Mostrar cuantas solicitudes hay en un animal.
		boolean realizarSolic=true;
		for(int i=0;i<nAnimales;i++) {
			if(listaAnimales[i].getNombre().equalsIgnoreCase(nombreA)) {
				realizarSolic=listaAnimales[i].realizarSolicitud(); //Almacena las solicitudes e impide que supere el numero maximo de solicitudes establecidas
			}
		}
		return realizarSolic;
	}

	public int consultarSolicitudesAdop(String nombreA) { // Este metodo se encaga de comprobar las solicitudes tiene el animal asociado.
		int nSolicitudes=0;
		for(int i=0;i<nAnimales;i++) {
			if(listaAnimales[i].getNombre().equalsIgnoreCase(nombreA)) { // Recorremos la matriz buscando el nombre introducido diferenciando entre mayusculas y minusculas
				nSolicitudes=listaAnimales[i].consultarSolicitudes(); // Comprueba cuantas solicitudes tiene el animal asociado.
			}
		}
		return nSolicitudes;
	}
	
	public String devolverSolicitudes(String nombreA){ //Este metodo se encarga de imprimir todas las solicitudesa asignadas a un animal.
		String info="";
		for(int i=0;i<nAnimales;i++) {
			if(listaAnimales[i].getNombre().equalsIgnoreCase(nombreA)) {
				info=listaAnimales[i].devolverSolicitudes();
			}
		}
		return info;
	}

	public double calcularTotalGastosVeterinariosAnuales() { // Hace un sumatorio de todos los gastos realizados a los perros y a los gatos.
		double total=0;
		for(int i=0;i<nAnimales;i++) {
			total+=listaAnimales[i].calculoGastosVetAnual();
		}
		return total;
	}

	public double calculoCampaniaEsterilizacion(ClinicaVeterinaria cv) { //Recorre la matriz para ver la cantidad de gatas que hay Esterilizadas
		int contador=0;
		for (int i=0;i<nAnimales;i++) {
			if (listaAnimales[i] instanceof Gato) {
				if (((Gato) listaAnimales[i]).getEsterilizado()==true) {
					contador++;
				}
			}
		}
		double precio=contador*cv.getPrecioEstGata();
		return precio;
	}

	public double calcularCantidadPiensoAdultos() { //Calcular la cantidad de pienso total que se va a suministrar a todos perros.
		double cantidad=0;
		for (int i=0;i<nAnimales;i++) {
			if (listaAnimales[i] instanceof Perro) {
				cantidad+=((Perro) listaAnimales[i]).calculoPiensoPerro()*7/1000;  // Calcula la cantidad asignada a cada perro segun su peso, y lo multiplica por 7 al ser durante una semana.
			}
		}
		return cantidad;
	}

	public double calcularSubvencionAyuntamiento(Ayuntamiento ay) { //Calcular el total de subvencion que va a proporcionar el ayuntamiento.
		double cantSubTotal,cantAdPorA,cantAdTotal;
		cantAdPorA=ay.getCantSub();
		cantAdTotal=cantAdPorA*nAnimales;
		cantSubTotal=SUBVENCION_AYUNTAF+cantAdTotal;
		return cantSubTotal;
	}
}	
