package Persistencia;

public class FueraIntervaloException extends Exception {
	public  FueraIntervaloException() {
	}

	public FueraIntervaloException(String texto) {
		super(texto);
	}
}