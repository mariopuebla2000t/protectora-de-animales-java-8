package Persistencia;

public interface Interfaz {

	public int MAX_SOLICITUDES=10;
	public int TRATMENSUAL_LEISHMANIA=25;
	public int VACUNAANUAL_RABIA=30;
	public int SEDACION_PERRO_NS=8; 
	public int TRAT_CELO_GATOS=10; 
	public int SUBVENCION_AYUNTAF=1000; 
	public int TAMANIOPERROPEQUENIO = 15;
	public int TAMANIOPERROGRANDE = 25;
	public int PIENSOPERROPEQUENIO = 200;
	public int PIENSOPERROMEDIANO = 300;
	public double PORCENTAJEPERROGRANDE = 1.5;
	
}