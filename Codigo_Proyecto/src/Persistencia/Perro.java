package Persistencia;

public class Perro extends Animal {
	
	String raza;
	double tamanioKg;
	boolean ppp;
	boolean leishmania;
	
	Perro(String nombre, char sexo, int anios, boolean sociablePersonas, boolean apadrinado, String raza, double tamanioKg, boolean ppp, boolean leishmania) {
		
		super(nombre, sexo, anios, sociablePersonas, apadrinado);
		this.raza = raza;
		this.tamanioKg = tamanioKg;
		this.ppp = ppp;
		this.leishmania = leishmania;
		
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public double getTamanioKg() {
		return tamanioKg;
	}

	public void setTamanioKg(double tamanioKg) {
		this.tamanioKg = tamanioKg;
	}

	public boolean getPpp() {
		return ppp;
	}

	public void setPpp(boolean ppp) {
		this.ppp = ppp;
	}

	public boolean getLeishmania() {
		return leishmania;
	}

	public void setLeishmania(boolean leishmania) {
		this.leishmania = leishmania;
	}
	
	public double calculoPiensoPerro() { // Calcula la cantidad asignada a cada perro segun su peso.
		double piensoPerro=0;
		if(anios > 1.5) {
		if(tamanioKg<=TAMANIOPERROPEQUENIO) {
			piensoPerro=PIENSOPERROPEQUENIO;
		}else if(TAMANIOPERROPEQUENIO<tamanioKg && tamanioKg<TAMANIOPERROGRANDE) {
			piensoPerro=PIENSOPERROMEDIANO;
		}else {
			piensoPerro=(tamanioKg * 1000)*(PORCENTAJEPERROGRANDE/100);
		}
	}
		return piensoPerro;
}
	
	public double calculoGastosVetAnual() { // Este es el metodo sobreescrito del que se hablaba en la clase protectora que utiliza una formula para calcular el precio.
		double gastosVetAnual=0,gastosVetLeish=0,gastosVetSed=0;
		if(apadrinado==false) {
			gastosVetAnual=VACUNAANUAL_RABIA;
			if(leishmania==true) {
				gastosVetLeish=TRATMENSUAL_LEISHMANIA*12;
			}
			gastosVetAnual+=gastosVetLeish;
			if(ppp==true & sociablePersonas==false) {
				gastosVetSed=SEDACION_PERRO_NS;
			}
			gastosVetAnual+=gastosVetSed;
		}
		return gastosVetAnual;
	}
	public String toString() {
		return "Perro (Nombre : "+nombre+", Sexo : "+sexo+", A�os : "+anios+", Sociable : "+sociablePersonas+", Apadrinado : "+apadrinado+", Raza : "+raza+", Peso : "+tamanioKg+", Perro PP : "+ppp+", Leishmania : "+leishmania+")";
	}
}
