package Persistencia;

public class Ayuntamiento {
	
	int numContacto;
	double cantSub;
	
	Ayuntamiento(int numContacto, double cantSub){
		
		this.numContacto=numContacto;
		this.cantSub=cantSub;
		
	}
	
	public int getNumContacto() {
		return numContacto;
	}

	public void setNumContacto(int numContacto) {
		this.numContacto = numContacto;
	}

	public double getCantSub() {
		return cantSub;
	}

	public void setCant_sub(double cantSub) {
		this.cantSub = cantSub;
	}

	public String toString() {
		return "Ayuntamiento de la ciudad:\nN� Telefono: "+numContacto+". Cantidad por animal recogido: "+cantSub+" �";
	}
	
}
