# Proyecto de Gestión de Protectora de Animales - Java 8
Proyecto desarrollado como parte de la asignatura **[42306	- Fundamentos de programación II](https://guiae.uclm.es/vistaGuia/407/42306)** en el primer curso de ingeniería informática. En este trabajo, se ha creado un sistema simple de gestión para una protectora de animales utilizando Java 8 y la POO (Programación Orientada a Objetos).

## Tecnologías Utilizadas
**Lenguaje de programación**: Java 8  
**Herramientas de desarrollo**: Eclipse IDE  

Más información en los PDF's asociados.
